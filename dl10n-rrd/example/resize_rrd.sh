#!/bin/sh

set -e

# This is just an example on how the rrd files can be enlarged.
# When I did it, the rrd file already lost some data, so I had to merge
# different sources 'see the commented parts)

find debian-l10n-stats/ -name "*.rrd" | while read file
do
#	dump=dump
#	olddump=olddump
#	newdump=newdump
#	rm -f $dump $olddump $newdump
#
#	oldfile=www-rrd${file#debian-l10n-stats}
#
#	if [ -f $oldfile ]; then
#		rrdtool dump $file    > $dump
#		rrdtool dump $oldfile > $olddump
#
#		head -n 73  $dump                   >> $newdump
#		tail -n +74 $olddump | grep "2007-" >> $newdump
#		tail -n +74 $dump | grep -v "2007-" >> $newdump
#		rm -f $file
#		rrdtool restore $newdump $file
#	fi

	rrdtool resize $file 0 GROW 700
done

