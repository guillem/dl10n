#!/usr/bin/perl

# manpages-rrd.pl -- Debian l10n manpages statistics (rrd format)
#
# Copyright (C) 2007 Nicolas François
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# Usage: manpages-rrd.pl <suite>
#        suite: testing, unstable

my $MIRROR="http://mirror.it.ubc.ca/debian/";

my $RRD_DATE;

my $dist = $ARGV[0];

my @archs;
if ($dist eq "stable") {
    @archs = qw/amd64 arm64 armel armhf i386 mips mips64el mipsel ppc64el s390x/;
} elsif ($dist eq "testing") {
    @archs = qw/amd64 arm64 armel armhf i386 mips mips64el mipsel ppc64el s390x/;
} elsif ($dist eq "unstable") {
    @archs = qw/amd64 arm64 armel armhf hurd-i386 i386 kfreebsd-amd64 kfreebsd-i386 mips mips64el mipsel powerpc ppc64el s390x/;
}

my %mans;

for my $arch (@archs) {
    if (-f "Contents-$arch.gz") {
        unlink "Contents-$arch.gz";
    }
    system ("wget", "-c", "--quiet", "$MIRROR/dists/$dist/main/Contents-$arch.gz") == 0
        or die "wget failed to download $MIRROR/dists/$dist/main/Contents-$arch.gz .  Aborting.";

    die "Missing Contents-$arch.gz .  Aborting."
        unless -f "Contents-$arch.gz";

    open(CONTENT, "gunzip -c 'Contents-$arch.gz'|")
        or die "Cannot open 'Contents-$arch.gz'";

    while(<CONTENT>) {
        if ($_ =~ m|^usr/share/man/(?:(.*?)/)?(man[0-9]/.*?)[\t ]+(.*)$|) {
            my $lang=$1||"_";
            my $man=$2;
            my $pkgs=$3;
            $man =~ s/\.gz$//;
            $mans{$man}{$lang}.=",$pkg";
        }
    }

    close(CONTENT)
        or die "Cannot close 'Contents-$arch.gz'";
    unlink "Contents-$arch.gz";
}

my %total;
my %total_translated_only;
my $lang;

foreach my $man (keys %mans) {
    if (defined $mans{$man}{'_'}) {
        foreach $lang (keys %{$mans{$man}}) {
            $total{$lang} += 1;
            $langs{$lang} = 1;
        }
    } else {
        $total_translated_only{'_'} += 1;
        foreach $lang (keys %{$mans{$man}}) {
            $total_translated_only{$lang} += 1;
            $langs{$lang} = 1;
#            print "no English page: $man ($lang)\n";
        }
    }
}

my $step = 60*60*24; # 1 day
if ( ! -d "man") {
    mkdir "man";
}

foreach $lang (sort keys %langs) {
    if ( ! -f "man/$lang.rrd") {
        system "rrdtool create man/$lang.rrd ".
               "--step $step ".
               ((not defined $RRD_DATE or $RRD_DATE eq "N")?"":"--start ".($RRD_DATE)." ").
               "DS:translated:GAUGE:".($step*1.5).":U:U ".
               "DS:only:GAUGE:".($step*1.5).":U:U ".
               "RRA:AVERAGE:0.5:1:700";
    }
    my $date="N";
    if (defined $RRD_DATE) {
        if ($RRD_DATE ne "N") {
            $date = $RRD_DATE+1;
        }
    } else {
        use POSIX qw(strftime);
        $date = strftime "%s", localtime;
        $date = (int($date / $step)+1)*$step
    }
    system "rrdtool update man/$lang.rrd $date:".
                                         $total{$lang}.":".
                                         $total_translated_only{$lang};
}


