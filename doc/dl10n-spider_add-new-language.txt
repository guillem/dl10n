0. Request support for new language
   

1. Add support for the new language in the dl10n tools
  * lib/Debian/L10n/Utils.pm
    Add the new language to %LanguageList and %Language
  * Changelog
    Document the above

2. Commit

3. Update the i18n.d.o working copy (/srv/dl10n-stuff/svn/dl10n)
   svn up

4. Initialize the database
   There are 2 options (a and b below)
   You need to check the year (YYYY below) and month (MM or M below) when
   pseudo URLs started to be used.
   Notye that the tool does not support gaps (i.e. when no messages are
   received in a month)

   a. Initialize the database with dl10n-spider
   In /srv/dl10n-stuff/svn/dl10n
   PERLLIB=lib ./dl10n-spider --year=YYYY --month=M --message=0 <ll>

   b. Create data/status.<ll>
== 8< ===============================================================
Package:
Date: YYYY-MM-01
Year: YYYY
Month: M
Message: 0
Page: 1

== 8< ===============================================================

5. Set the permission to the database
   sudo chown debian-i18n-robots: data/status.<ll>

6. Add support for the language in the cron job (/srv/dl10n-stuff/bin/spiderbts)
   mkdir -p html/{...}
   for language in ...
   cp -r html/{...}

   Apply the same to spiderhourly2 (no more used, but ...)

7. Add db initialization to spiderinit
   spiderinit.2006 with the date of initial usage of pseudourl
   spiderinit with the date of craetion of the mailing list

   Multiple lines are needed if there are gaps.

   (spiderinit is useful in case of db corruption; spiderinit.2006 speeds
   the process by focusing to the period with pseudourl usage)

8. Check
   spiderbts is run every hour
   logs are present in /srv/dl10n-stuff/log/spiderbts.cron.<date-hour>

9. Update /srv/i18n.debian.net/www/index.html
